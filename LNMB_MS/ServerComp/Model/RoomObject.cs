﻿using ServerComp;
using ServerComp.DSC;
using ServerComp.MatchMaking;
using ServerComp.Player;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerComp.Model
{
    public enum E_RoomState
    {
        WaitingForDSC,
        WaitingForDS,
        ReadyForPlayersToJoin,
        Playing
    }

    class RoomObject 
    {
        public string RoomID;
        public E_MatchType matchType;

        public E_RoomState roomState;
        public Dictionary<string, PlayerConnection> players;

        // Constructor with DSC found
        public RoomObject(Dictionary<string, PlayerConnection> inPlayers, E_MatchType inMatchType)
        {
            roomState = E_RoomState.WaitingForDSC;

            players = inPlayers;
            matchType = inMatchType;

            foreach (KeyValuePair<string, PlayerConnection> con in players)
            {
                con.Value.OnDisconnected += HandlePlayerDisconnected;
            }

            RoomID = Server.GetRoomID();
        }

        // Handle player disconnected
        public void HandlePlayerDisconnected(PlayerConnection playerConnection)
        {
            players.Remove(playerConnection.player.playerID);
        }

        public string GetJsonString()
        {
            string jsonString = "{\"roomID\":\"" + RoomID + "\"}";
            return jsonString;
        }

        public void SetRoomState(E_RoomState inRoomState)
        {
            roomState = inRoomState;
        }
    }
}
