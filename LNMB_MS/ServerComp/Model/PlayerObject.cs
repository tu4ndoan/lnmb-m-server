﻿using ServerComp.Player;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerComp.Model
{
    class PlayerObject
    {
        public string playerID;

        public PlayerObject(string inPlayerID)
        {
            playerID = inPlayerID;
        }
    }
}
