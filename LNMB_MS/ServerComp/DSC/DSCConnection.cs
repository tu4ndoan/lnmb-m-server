﻿using Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using ServerComp.Model;

namespace ServerComp.DSC
{
    class DSCConnection
    {
        public Socket socket;
        public Thread receiveDataThread;
        public bool bWantStopReceivingData;
        public bool bAvailable;
        public bool bAccepted;
        public Server server;
        public string dscID;

        // Keep track of assigned rooms for this DSC
        Dictionary<string, RoomObject> assignedRooms;

        // Keep connection timer
        public System.Timers.Timer KeepConnectionTimer;

        // Constructor
        public DSCConnection(Socket inSocket, Server inServer)
        {
            assignedRooms = new Dictionary<string, RoomObject>();

            SetAvailable(true);
            SetAccepted(false);

            server = inServer;
            bWantStopReceivingData = false;
            socket = inSocket;

            // Start listen to DSC
            StartListeningToDSC();

            // Keep connection
            KeepConnection();
        }

        public void SetAvailable(bool Value)
        {
            bAvailable = Value;
        }

        public void SetAccepted(bool Value)
        {
            bAccepted = Value;
        }

        public bool AreYouAvailable()
        {
            return bAvailable && bAccepted;
        }

        public void PlayThisRoom(RoomObject room)
        {
            // Change to new state
            room.SetRoomState(E_RoomState.WaitingForDS);

            // Add to assigned rooms
            assignedRooms.Add(room.RoomID, room);

            // Send to DSS
            server.dscController.ToDSC_PlayThisRoom(room, this);
        }      

        public void KeepConnection()
        {
            KeepConnectionTimer = new System.Timers.Timer(180000);
            KeepConnectionTimer.Elapsed += OnKeepConnectionEvent;
            KeepConnectionTimer.AutoReset = true;
            KeepConnectionTimer.Enabled = true;
        }

        private void OnKeepConnectionEvent(Object source, ElapsedEventArgs e)
        {
            // Send nothing to keep the connection
            Packet packet = new Packet(-1, "");
            packet.Send(socket, false);
        }

        public void StartListeningToDSC()
        {
            receiveDataThread = new Thread(server.dscController.ReceiveDataFromDSC);
            receiveDataThread.Start(this);
        }

        public void HandleDisconnected()
        {
            if (socket != null)
            {
                // Close connection
                socket.Close();
                socket.Dispose();               
            }

            if (KeepConnectionTimer != null)
            {
                KeepConnectionTimer.Stop();
                KeepConnectionTimer.Dispose();
            }

            // Kill the thread lastly
            bWantStopReceivingData = true;
            receiveDataThread.Abort();
        }
    }
}
