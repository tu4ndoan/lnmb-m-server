﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ServerComp.DSC
{
    class DSCListener
    {
        public Socket listeningToDSCsSocket;
        ConnectionInfo listeningToDSCsConnection;
        public Thread listeningToDSCsThread;
        public bool bWantStopListening;
        public Server server;

        public DSCListener(Server inServer)
        {
            bWantStopListening = false;

            server = inServer;
            listeningToDSCsConnection = new ConnectionInfo(server.serverInfo.IP, server.serverInfo.listenToDSCsPort);
            listeningToDSCsSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            listeningToDSCsSocket.Bind(listeningToDSCsConnection.IP_EndPoint);

            StartListeningToDSCs();
        }

        public void StartListeningToDSCs()
        {
            listeningToDSCsThread = new Thread(ListenToDSCsThread);
            listeningToDSCsThread.Start();

            Server.LOG("Ready for Dedicated Server Controllers to connect", E_LogCategory.Successful);
        }

        public void StopListening()
        {
            bWantStopListening = false;
        }

        public void ListenToDSCsThread()
        {
            while (bWantStopListening == false)
            {
                listeningToDSCsSocket.Listen(1);
                DSCConnection dscConnection = new DSCConnection(listeningToDSCsSocket.Accept(), server);
                server.dscController.AddNewDSCConnection(dscConnection);
                Server.LOG("<Dedicated Server Controller> connected", E_LogCategory.Log);
            }
        }
    }
}
