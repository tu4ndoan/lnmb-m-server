﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ServerComp.Player
{
    class PlayerListener
    {
        public Socket listeningToPlayersSocket;
        ConnectionInfo listeningToPlayersConnection;
        public Thread listeningToPlayersThread;
        public bool bWantStopListening;
        public Server server;

        public PlayerListener(Server inServer)
        {
            bWantStopListening = false;

            server = inServer;
            listeningToPlayersConnection = new ConnectionInfo(server.serverInfo.IP, server.serverInfo.listenToPlayersPort);
            listeningToPlayersSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            listeningToPlayersSocket.Bind(listeningToPlayersConnection.IP_EndPoint);

            StartListeningToPlayers();
        }

        public void StartListeningToPlayers()
        {
            listeningToPlayersThread = new Thread(ListenToPlayersThread);
            listeningToPlayersThread.Start();

            Server.LOG("Ready for Players to connect", E_LogCategory.Successful);
        }

        public void ListenToPlayersThread()
        {
            while (bWantStopListening == false)
            {
                listeningToPlayersSocket.Listen(1);
                PlayerConnection playerConnection = new PlayerConnection(listeningToPlayersSocket.Accept(), server);
                server.playerController.AddPlayerConnection(playerConnection);
                Server.LOG("<Player> connected", E_LogCategory.Log);
            }
        }
    }
}
