﻿using Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using ServerComp.Model;

namespace ServerComp.Player
{
    class PlayerConnection
    {
        public Socket socket;
        public Thread receiveDataThread;
        public bool bWantStopReceivingData;
        public bool bAccepted = false;
        public Server server;

        // Store player info related to this connection
        public PlayerObject player;

        // Kêp connection timer
        public System.Timers.Timer KeepConnectionTimer;

        // Delegates
        public delegate void DisconnectedDelegate(PlayerConnection playerConnection);

        // Events
        public event DisconnectedDelegate OnDisconnected;

        // Constructor
        public PlayerConnection(Socket inSocket, Server inServer)
        {
            server = inServer;
            bWantStopReceivingData = false;
            socket = inSocket;
            StartListeningToPlayer();
        }

        public void StartListeningToPlayer()
        {
            receiveDataThread = new Thread(server.playerController.ReceiveDataFromPlayer);
            receiveDataThread.Start(this);

            KeepConnection();
        }

        public void KeepConnection()
        {
            KeepConnectionTimer = new System.Timers.Timer(180000);
            KeepConnectionTimer.Elapsed += OnKeepConnectionEvent;
            KeepConnectionTimer.AutoReset = true;
            KeepConnectionTimer.Enabled = true;
        }

        private void OnKeepConnectionEvent(Object source, ElapsedEventArgs e)
        {
            // Send nothing to keep the connection
            Packet packet = new Packet(-1, "");
            packet.Send(socket, false);
        }

        public void HandleDisconnected()
        {           
            if (socket != null)
            {
                // Close connection
                socket.Close();
                socket.Dispose();          
            }

            if (KeepConnectionTimer != null)
            {
                KeepConnectionTimer.Stop();
                KeepConnectionTimer.Dispose();
            }

            if (OnDisconnected != null)
            {
                OnDisconnected(this);
            }

            // Kill the thread lastly
            bWantStopReceivingData = true;
            receiveDataThread.Abort();
        }
    }
}
