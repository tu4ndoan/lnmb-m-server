﻿using ServerComp.MappingObject.Player;
using ServerComp.Player;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerComp.MatchMaking
{
    class MatchMakingRequest
    {
        public PlayerConnection playerConnection;
        public List<E_MatchType> matchTypes;

        public MatchMakingRequest(PlayerConnection inPlayerConnection, Player_Action_1 data)
        {
            matchTypes = new List<E_MatchType>();
            playerConnection = inPlayerConnection;

            foreach(int item in data.matchTypes)
            {
                matchTypes.Add((E_MatchType)item);
            }
        }
    }
}
