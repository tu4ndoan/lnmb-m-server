﻿using ServerComp.Model;
using ServerComp;
using ServerComp.DSC;
using ServerComp.Player;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerComp.MatchMaking
{
    public enum E_MatchType
    {
        TeamDeathMatch = 0,
        KingOfTheHill = 1,
        DeathMatch = 2
    }

    class MatchMakingService
    {
        public Server server;

        Dictionary<string, MatchMakingRequest> mm_TeamDeathMatch_Requests;
        Dictionary<string, MatchMakingRequest> mm_KingOfTheHill_Requests;
        Dictionary<string, MatchMakingRequest> mm_DeathMatch_Requests;

        Dictionary<string, RoomObject> waitingForDSC;
        Dictionary<string, RoomObject> waitingForDS;
        Dictionary<string, RoomObject> playingRooms;

        public MatchMakingService(Server inServer)
        {
            server = inServer;

            mm_TeamDeathMatch_Requests = new Dictionary<string, MatchMakingRequest>();
            mm_KingOfTheHill_Requests = new Dictionary<string, MatchMakingRequest>();
            mm_DeathMatch_Requests = new Dictionary<string, MatchMakingRequest>();

            waitingForDSC = new Dictionary<string, RoomObject>();
            waitingForDS = new Dictionary<string, RoomObject>();
            playingRooms = new Dictionary<string, RoomObject>();
        }

        public void HandlePlayerDisconnected(PlayerConnection playerConnection)
        {
            mm_TeamDeathMatch_Requests.Remove(playerConnection.player.playerID);
            mm_KingOfTheHill_Requests.Remove(playerConnection.player.playerID);
            mm_DeathMatch_Requests.Remove(playerConnection.player.playerID);
        }

        public void AddMatchMakingRequest(MatchMakingRequest mmRequest)
        {
            // Check consistence
            if(mmRequest.matchTypes.Count() <= 0)
            {
                server.playerController.ToPlayer_CreateMatchMakingRequestResponse(false, "Create match making failed", mmRequest.playerConnection);
            }

            // Prepare somethings before find available match
            mmRequest.playerConnection.OnDisconnected += HandlePlayerDisconnected;

            // Add request to correspondent lists
            foreach (E_MatchType matchType in mmRequest.matchTypes)
            {
                switch (matchType)
                {
                    case E_MatchType.TeamDeathMatch:
                        mm_TeamDeathMatch_Requests.Add(mmRequest.playerConnection.player.playerID, mmRequest);
                        break;
                    case E_MatchType.KingOfTheHill:
                        mm_KingOfTheHill_Requests.Add(mmRequest.playerConnection.player.playerID, mmRequest);
                        break;
                    case E_MatchType.DeathMatch:
                        mm_DeathMatch_Requests.Add(mmRequest.playerConnection.player.playerID, mmRequest);
                        break;
                }
            }

            // Accept the request
            server.playerController.ToPlayer_CreateMatchMakingRequestResponse(true, "Create match making successful", mmRequest.playerConnection);

            FindMatchForTheRequest(mmRequest);
        }

        public bool FindMatchForTheRequest(MatchMakingRequest mmRequest)
        {
            int tempEnoughNumOfPlayers = 2;
            bool bFoundMatch = false;

            Dictionary<string, MatchMakingRequest> selectedList = new Dictionary<string, MatchMakingRequest>();
            E_MatchType matchTypeFound = E_MatchType.DeathMatch;

            foreach (E_MatchType matchType in mmRequest.matchTypes)
            {
                switch (matchType)
                {
                    case E_MatchType.TeamDeathMatch:
                        if (mm_TeamDeathMatch_Requests.Count() == tempEnoughNumOfPlayers)
                        {
                            bFoundMatch = true;
                            matchTypeFound = E_MatchType.TeamDeathMatch;
                            selectedList = mm_TeamDeathMatch_Requests;
                        }
                        break;
                    case E_MatchType.KingOfTheHill:
                        if (mm_KingOfTheHill_Requests.Count() == tempEnoughNumOfPlayers)
                        {
                            bFoundMatch = true;
                            matchTypeFound = E_MatchType.KingOfTheHill;
                            selectedList = mm_KingOfTheHill_Requests;
                        }
                        break;
                    case E_MatchType.DeathMatch:
                        if (mm_DeathMatch_Requests.Count() == tempEnoughNumOfPlayers)
                        {
                            bFoundMatch = true;
                            matchTypeFound = E_MatchType.DeathMatch;
                            selectedList = mm_DeathMatch_Requests;
                        }
                        break;
                }

                if (bFoundMatch)
                {
                    break;
                }
            }

            if (bFoundMatch)
            {
                Dictionary<string, PlayerConnection> selectedPlayers = new Dictionary<string, PlayerConnection>();

                foreach (KeyValuePair<string, MatchMakingRequest> request in selectedList)
                {
                    selectedPlayers.Add(request.Key, request.Value.playerConnection);
                }

                // Remove all selected players from waiting list
                foreach (KeyValuePair<string, PlayerConnection> request in selectedPlayers)
                {
                    selectedList.Remove(request.Value.player.playerID);
                }

                // Create a room
                return CreateRoom(selectedPlayers, matchTypeFound);
            }
            else
            {
                // Can not create a room yet, need to wait
                return false;
            }
        }

        public bool CreateRoom(Dictionary<string, PlayerConnection> players, E_MatchType matchType)
        {
            // Construct a room
            RoomObject room = new RoomObject(players, matchType); // roomID is created from here

            Server.LOG("<RoomID = " + room.RoomID + "> created", E_LogCategory.Warning);

            // Find available DSC
            DSCConnection dscConnection = server.dscController.FindAvailableDSC();

            // If found DSC => Assign this room to that dsc
            if (dscConnection != null)
            {
                AssignRoomToDSC(room, dscConnection);
            }
            else
            {
                waitingForDSC.Add(room.RoomID, room);
            }

            return true;
        }

        public void HandleNewDSCConnected(DSCConnection dscConnection)
        {
            if(waitingForDSC.Count() > 0)
            {
                AssignRoomToDSC(waitingForDSC.First().Value, dscConnection);

                // Finally remove from waiting for DSC
                waitingForDSC.Remove(waitingForDSC.First().Value.RoomID);
            }
        }

        public void AssignRoomToDSC(RoomObject room, DSCConnection dscConnection)
        {
            dscConnection.PlayThisRoom(room);
            waitingForDS.Add(room.RoomID, room);
        }

        public void ThisRoomIsReadyToPlay(string roomID, string IP, int port)
        {
            // Set new state
            waitingForDS[roomID].SetRoomState(E_RoomState.ReadyForPlayersToJoin);

            // Send info to all player to join
            foreach(KeyValuePair<string, PlayerConnection> con in waitingForDS[roomID].players)
            {
                server.playerController.ToPlayer_PlayThisRoom(waitingForDS[roomID], con.Value, IP, port);
            }

            // Add to playing rooms
            playingRooms.Add(roomID, waitingForDS[roomID]);

            // remove from waiting for DS
            waitingForDS.Remove(roomID);
        }
    }
}
