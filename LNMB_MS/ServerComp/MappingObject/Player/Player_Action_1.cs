﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Utilities;

namespace ServerComp.MappingObject.Player
{
    [DataContract]
    class Player_Action_1
    {
        [DataMember(Name = "matchTypes")]
        public int[] matchTypes;

        public Player_Action_1()
        {

        }

        public static void Convert(Packet packet, out Player_Action_1 targetInstance)
        {
            targetInstance = new Player_Action_1();
            MemoryStream ms = new MemoryStream(Encoding.ASCII.GetBytes(packet.data));
            DataContractJsonSerializer jsonData = new DataContractJsonSerializer(targetInstance.GetType());
            targetInstance = jsonData.ReadObject(ms) as Player_Action_1;
        }
    }
}
