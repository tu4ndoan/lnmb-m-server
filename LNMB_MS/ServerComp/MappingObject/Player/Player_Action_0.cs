﻿using Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace ServerComp.MappingObject.Player
{
    [DataContract]
    class Player_Action_0
    {
        [DataMember(Name = "secret")]
        public string secretKey = "";

        [DataMember(Name = "pid")]
        public string playerID = "";

        public Player_Action_0()
        {

        }

        public static void Convert(Packet packet, out Player_Action_0 targetInstance)
        {
            targetInstance = new Player_Action_0();
            MemoryStream ms = new MemoryStream(Encoding.ASCII.GetBytes(packet.data));
            DataContractJsonSerializer jsonData = new DataContractJsonSerializer(targetInstance.GetType());
            targetInstance = jsonData.ReadObject(ms) as Player_Action_0;
        }
    }
}
