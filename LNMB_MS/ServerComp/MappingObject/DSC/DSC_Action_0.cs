﻿using Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace ServerComp.MappingObject.DSC
{
    [DataContract]
    class DSC_Action_0 : MasterJSON
    {
        [DataMember(Name = "secret")]
        public string secretKey = "";

        public DSC_Action_0()
        {

        }

        public static void Convert(Packet packet, out DSC_Action_0 targetInstance)
        {
            targetInstance = new DSC_Action_0();
            MemoryStream ms = new MemoryStream(Encoding.ASCII.GetBytes(packet.data));
            DataContractJsonSerializer jsonData = new DataContractJsonSerializer(targetInstance.GetType());
            targetInstance = jsonData.ReadObject(ms) as DSC_Action_0;
        }
    }
}
