﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Utilities;

namespace ServerComp.MappingObject.DSC
{
    [DataContract]
    class DSC_Action_1
    {
        [DataMember(Name = "IP")]
        public string IP = "";

        [DataMember(Name = "port")]
        public int port;

        [DataMember(Name = "roomID")]
        public string roomID = "";

        public DSC_Action_1()
        {

        }

        public static void Convert(Packet packet, out DSC_Action_1 targetInstance)
        {
            targetInstance = new DSC_Action_1();
            MemoryStream ms = new MemoryStream(Encoding.ASCII.GetBytes(packet.data));
            DataContractJsonSerializer jsonData = new DataContractJsonSerializer(targetInstance.GetType());
            targetInstance = jsonData.ReadObject(ms) as DSC_Action_1;
        }
    }
}
