﻿using ServerComp.Controller;
using ServerComp.DSC;
using ServerComp.MappingObject.DSC;
using ServerComp.MappingObject.Player;
using ServerComp.Player;
using Utilities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using ServerComp.MatchMaking;

namespace ServerComp
{
    public struct ServerInfo
    {
        public int listenToPlayersPort;
        public int listenToDSCsPort;
        public string IP;

        public ServerInfo(string ip, int listenToClientsPort, int listenToDSCsPort)
        {
            IP = ip;
            this.listenToPlayersPort = listenToClientsPort;
            this.listenToDSCsPort = listenToDSCsPort;
        }
    }

    public struct ConnectionInfo
    {
        public IPEndPoint IP_EndPoint;

        public ConnectionInfo(string IP, int port)
        {
            IP_EndPoint = new IPEndPoint(IPAddress.Parse(IP), port);
        }
    }

    public enum E_LogCategory
    {
        Log, Warning, Error, Successful
    }

    class Server
    {
        private static int CurrentRoomNumber = 0;
        private static int CurrentDSCNumber = 0;

        // ----------------------------------------------------------------------------------------------------

        public ServerInfo serverInfo;
        public ServerConfig serverConfig;

        // ----------------------------------------------------------------------------------------------------

        public DSCController dscController;
        public PlayerController playerController;
        public MatchMakingService matchMakingService;

        // ----------------------------------------------------------------------------------------------------

        public Server(ServerConfig inServerConfig)
        {
            serverConfig = inServerConfig;
            serverInfo = new ServerInfo(serverConfig.thisServerIP, serverConfig.listenPlayerPort, serverConfig.listenDSCPort);

            playerController = new PlayerController(this);
            dscController = new DSCController(this);
            matchMakingService = new MatchMakingService(this);
        }

        public void Start()
        {
            playerController.Start();
            dscController.Start();

            LOG("<Master Server> started", E_LogCategory.Successful);
        }

        public static void LOG(string message, E_LogCategory logCategory)
        {
            StackFrame callStack = new StackFrame(1, true);
            string Time = DateTime.Now.ToString();

            switch (logCategory)
            {
                case E_LogCategory.Log:
                    Console.ForegroundColor = ConsoleColor.White;
                    break;
                case E_LogCategory.Warning:
                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                    break;
                case E_LogCategory.Error:
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
                case E_LogCategory.Successful:
                    Console.ForegroundColor = ConsoleColor.Green;
                    break;
            }

            Console.WriteLine(Time + " - " + callStack.GetFileName().Split('\\').Last() + " (" + callStack.GetFileLineNumber() + "): " + message);

            Console.ResetColor();
        }

        public static string GetRoomID()
        {
            string RoomID = DateTime.Now.TimeOfDay.ToString().Replace(":", "").Replace("/", "") + "." + CurrentRoomNumber.ToString();
            CurrentRoomNumber++;
            return RoomID;
        }

        public static string GetDSCID()
        {
            string DSCID = DateTime.Now.TimeOfDay.ToString().Replace(":","").Replace("/","") + "." + CurrentDSCNumber.ToString();
            CurrentDSCNumber++;
            return DSCID;
        }
    }
}
