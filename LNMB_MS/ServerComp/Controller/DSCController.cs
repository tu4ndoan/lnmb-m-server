﻿using ServerComp.DSC;
using ServerComp.MappingObject.DSC;
using ServerComp.Player;
using Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using ServerComp.Model;

namespace ServerComp.Controller
{
    public enum E_MSToDSCActions
    {
        TellMeYourState = 0,
        PlayThisRoomForMe = 1
    }

    class DSCController
    {
        public List<DSCConnection> checkingConnections;

        public Dictionary<string, DSCConnection> acceptedConnections;

        public Server server;

        public DSCController(Server inServer)
        {
            server = inServer;
            checkingConnections = new List<DSCConnection>();
            acceptedConnections = new Dictionary<string, DSCConnection>();
        }

        public void Start()
        {
            StartListeningToDSCs();
        }

        public void StartListeningToDSCs()
        {
            DSCListener dscListener = new DSCListener(server);
        }

        public void Disconnect(DSCConnection dscConnection)
        {
            // First remove from all lists
            RemoveDSCConnection(dscConnection);

            // Finally disconnect
            dscConnection.HandleDisconnected();
        }

        public void AddNewDSCConnection(DSCConnection dscConnection)
        {
            dscConnection.dscID = Server.GetDSCID();
            checkingConnections.Add(dscConnection);
            ToDSC_SendMeYourCurrentState(dscConnection);
        }

        public void RemoveDSCConnection(DSCConnection dscConnection)
        {
            if (dscConnection.bAccepted)
            {
                acceptedConnections.Remove(dscConnection.dscID);
            }
            else
            {
                checkingConnections.Remove(dscConnection);
            }
        }

        public void ReceiveDataFromDSC(object dscConnection)
        {
            DSCConnection dscCon = (DSCConnection)dscConnection;

            byte[] Buffer;
            int readBytes;

            while (dscCon.bWantStopReceivingData == false)
            {
                try
                {
                    Buffer = new byte[dscCon.socket.SendBufferSize];
                    readBytes = dscCon.socket.Receive(Buffer);
                    Array.Resize(ref Buffer, readBytes);

                    if (readBytes > 0)
                    {
                        Packet pk = new Packet(Buffer);
                        DataProcessorForDSC(pk, dscCon);
                    }
                    else
                    {
                        throw new SocketException();
                    }
                }
                catch (SocketException sEx)
                {
                    Server.LOG("<Dedicated Server Controller> disconnected | <Reason = Connection lost>", E_LogCategory.Error);
                    Disconnect(dscCon);
                    break;
                }
            }
        }

        public void DataProcessorForDSC(Packet packet, DSCConnection dscConnection)
        {
            switch (packet.action)
            {
                case 0:
                    FromDSC_ReceiveCurrentState(packet, dscConnection);
                    break;
                case 1:
                    FromDSC_RoomIsReadyToPlay(packet, dscConnection);
                    break;
                default:
                    break;
            }
        }

        // ----------------------------------------------------------------------------------------------------

        // Following functions will handle all RESPONSES from DSCs
        // **************************************************************************************************** //

        public void FromDSC_ReceiveCurrentState(Packet packet, DSCConnection dscConnection)
        {
            DSC_Action_0 response = new DSC_Action_0();
            DSC_Action_0.Convert(packet, out response);

            if (response.secretKey.Equals(server.serverConfig.dscSecretKey))
            {       
                // Set connection as accepted
                dscConnection.SetAccepted(true);

                // Add to accepted connections
                acceptedConnections.Add(dscConnection.dscID, dscConnection);

                // Remove from checking connections
                checkingConnections.Remove(dscConnection);

                Server.LOG("Accepted", E_LogCategory.Successful);

                // Notify MMS
                server.matchMakingService.HandleNewDSCConnected(dscConnection);
            }
            else
            {
                Server.LOG("Not correct", E_LogCategory.Error);
            }
        }

        public void FromDSC_RoomIsReadyToPlay(Packet packet, DSCConnection dscConnection)
        {
            DSC_Action_1 response = new DSC_Action_1();
            DSC_Action_1.Convert(packet, out response);

            server.matchMakingService.ThisRoomIsReadyToPlay(response.roomID, response.IP, response.port);
            Server.LOG("<RoomID = "+ response.roomID + "> is ready to play", E_LogCategory.Warning);
        }

        // **************************************************************************************************** //
        // End ----------------------------------------------------------------------------------------------------

        // Following functions will handle all REQUESTS to DSCs
        // **************************************************************************************************** //

        public void ToDSC_SendMeYourCurrentState(DSCConnection dscConnection)
        {
            Packet packet = new Packet((int)E_MSToDSCActions.TellMeYourState, "");
            packet.Send(dscConnection.socket, false);
        }

        public void ToDSC_PlayThisRoom(RoomObject room, DSCConnection dscConnection)
        {
            Packet packet = new Packet((int)E_MSToDSCActions.PlayThisRoomForMe, room.GetJsonString());
            packet.Send(dscConnection.socket, false);
        }

        // **************************************************************************************************** //
        // End ----------------------------------------------------------------------------------------------------

        public DSCConnection FindAvailableDSC()
        {
            foreach(KeyValuePair<string, DSCConnection> dsc in acceptedConnections)
            {
                if(dsc.Value.AreYouAvailable())
                {
                    return dsc.Value;
                }
            }

            return null;
        }
    }
}
