﻿using ServerComp.MappingObject.Player;
using ServerComp.Player;
using Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using ServerComp.Model;
using ServerComp.MatchMaking;
using ServerComp.Model;

namespace ServerComp.Controller
{
    public enum E_MSToPlayerActions
    {
        TellMeWhoAreYou = 0,
        SendInitialInfoResponse = 1,
        CreateMatchMakingRequestResponse = 2,
        PlayThisRoom = 3
    }

    class PlayerController
    {
        public List<PlayerConnection> playerConnections;

        public Dictionary<string, PlayerConnection> acceptedConnections;

        public Server server;

        public PlayerController(Server inServer)
        {
            server = inServer;
            playerConnections = new List<PlayerConnection>();
            acceptedConnections = new Dictionary<string, PlayerConnection>();
        }

        public void Start()
        {
            StartListeningToPlayers();
        }

        public void StartListeningToPlayers()
        {
            PlayerListener playerListener = new PlayerListener(server);
        }

        public void AddPlayerConnection(PlayerConnection playerConnection)
        {
            playerConnections.Add(playerConnection);
            ToPlayer_TellMeWhoAreYou(playerConnection);   
        }

        public void RemovePlayerConnection(PlayerConnection playerConnection)
        {
            if(playerConnection.bAccepted)
            {
                acceptedConnections.Remove(playerConnection.player.playerID);
            }
            else
            {
                playerConnections.Remove(playerConnection);
            }            
        }

        public void ReceiveDataFromPlayer(object playerConnection)
        {
            PlayerConnection playerCon = (PlayerConnection)playerConnection;

            byte[] Buffer;
            int readBytes;

            while (playerCon.bWantStopReceivingData == false)
            {
                try
                {
                    Buffer = new byte[playerCon.socket.SendBufferSize];
                    readBytes = playerCon.socket.Receive(Buffer);
                    Array.Resize(ref Buffer, readBytes);

                    if (readBytes > 0)
                    {
                        Packet pk = new Packet(Buffer);
                        DataProcessorForPlayer(pk, playerCon);
                    }
                    else
                    {
                        throw new SocketException();
                    }
                }
                catch (SocketException sEx)
                {
                    Server.LOG("<Player> disconnected | <Reason = Connection lost>", E_LogCategory.Error);
                    Disconnect(playerCon);
                    break;
                }
            }
        }

        public void DataProcessorForPlayer(Packet packet, PlayerConnection playerConnection)
        {
            switch (packet.action)
            {
                case 0:
                    FromPlayer_ReceiveInitialInfoFromPlayer(packet, playerConnection);
                    break;
                case 1:
                    FromPlayer_CreateMatchMakingRequest(packet, playerConnection);
                    break;
                default:
                    break;
            }
        }

        // ----------------------------------------------------------------------------------------------------

        // Following functions will handle all RESPONSES from Players
        // **************************************************************************************************** //

        public void FromPlayer_ReceiveInitialInfoFromPlayer(Packet packet, PlayerConnection playerConnection)
        {
            Player_Action_0 response = new Player_Action_0();
            Player_Action_0.Convert(packet, out response);

            bool bAccepted = response.secretKey.Equals(server.serverConfig.playerSecretKey);
            string message = "";

            if (bAccepted)
            {
                // Create new player
                PlayerObject player = new PlayerObject(response.playerID + "." + new Random().Next(1000000));
                playerConnection.player = player;
                playerConnection.bAccepted = true;

                try
                {
                    acceptedConnections.Add(playerConnection.player.playerID, playerConnection);
                    playerConnections.Remove(playerConnection);

                    message = "Accepted";
                    Server.LOG("<PlayerID = " + player.playerID + "> accepted", E_LogCategory.Successful);
                }
                catch(Exception ex)
                {
                    Disconnect(playerConnection);
                }
            }
            else
            {
                message = "Secret key is correct";
                Server.LOG("Secret key is correct", E_LogCategory.Error);

                Disconnect(playerConnection);
            }

            ToPlayer_SendInitialInfoResponse(bAccepted, message, playerConnection);
        }

        public void Disconnect(PlayerConnection playerConnection)
        {
            // First remove from all lists
            RemovePlayerConnection(playerConnection);

            // Finally disconnect
            playerConnection.HandleDisconnected();
        }

        public void FromPlayer_CreateMatchMakingRequest(Packet packet, PlayerConnection playerConnection)
        {
            if(playerConnection.player != null)
            {
                Player_Action_1 response = new Player_Action_1();
                Player_Action_1.Convert(packet, out response);

                MatchMakingRequest mmRequest = new MatchMakingRequest(playerConnection, response);
                server.matchMakingService.AddMatchMakingRequest(mmRequest);
            }
            else
            {
                ToPlayer_CreateMatchMakingRequestResponse(false, "Not accepted", playerConnection);
                Disconnect(playerConnection);
            }
        }

        // **************************************************************************************************** //
        // End ----------------------------------------------------------------------------------------------------

        // Following functions will handle all REQUESTS to Players
        // **************************************************************************************************** //

        public void ToPlayer_TellMeWhoAreYou(PlayerConnection playerConnection)
        {
            Packet packet = new Packet((int)E_MSToPlayerActions.TellMeWhoAreYou, "");
            packet.Send(playerConnection.socket, true);
        }

        public void ToPlayer_SendInitialInfoResponse(bool bSuccessful, string message, PlayerConnection playerConnection)
        {
            string jsonString = "{\"response\": " + bSuccessful + ", \"message\":\"" + message + "\"}";
            Packet packet = new Packet((int)E_MSToPlayerActions.SendInitialInfoResponse, jsonString);
            packet.Send(playerConnection.socket, true);
        }

        public void ToPlayer_CreateMatchMakingRequestResponse(bool bSuccessful, string message, PlayerConnection playerConnection)
        {
            string jsonString = "{\"response\": " + bSuccessful + ", \"message\":\"" + message + "\"}";
            Packet packet = new Packet((int)E_MSToPlayerActions.CreateMatchMakingRequestResponse, jsonString);
            packet.Send(playerConnection.socket, true);
        }

        public void ToPlayer_PlayThisRoom(RoomObject room, PlayerConnection playerConnection, string IP, int port)
        {
            string jsonString = "{\"IP\": \"" + IP + "\", \"port\":" + port + "}";
            Packet packet = new Packet((int)E_MSToPlayerActions.PlayThisRoom, jsonString);
            packet.Send(playerConnection.socket, true);
        }

        // **************************************************************************************************** //
        // End ----------------------------------------------------------------------------------------------------

    }
}
