﻿using Utilities;
using ServerComp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LNMB_MS
{
    class Program
    {
        private static ServerConfig serverConfig;

        static void Main(string[] args)
        {
            try
            {
                LoadConfig();
                Server server = new Server(serverConfig);
                server.Start();
            }
            catch (Exception ex)
            {
                Server.LOG("Cannot read ServerConfig file", E_LogCategory.Error);
            }

            Console.ReadLine();
        }

        public static void LoadConfig()
        {
            string data;
            var fileStream = new FileStream(@"ServerConfig.txt", FileMode.Open, FileAccess.Read);
            using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
            {
                data = streamReader.ReadToEnd();
            }

            serverConfig = new ServerConfig();
            ServerConfig.Convert(data, out serverConfig);
        }
    }
}
