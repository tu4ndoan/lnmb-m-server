﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace Utilities
{
    class DataContractJsonSerializerHelper
    {
        public static string GetJson<T>(T obj, DataContractJsonSerializer serializer = null)
        {
            using (var memory = new MemoryStream())
            {
                (serializer ?? new DataContractJsonSerializer(typeof(T))).WriteObject(memory, obj);
                memory.Seek(0, SeekOrigin.Begin);
                using (var reader = new StreamReader(memory))
                {
                    return reader.ReadToEnd();
                }
            }
        }
    }
}
