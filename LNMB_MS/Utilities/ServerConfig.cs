﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace Utilities
{
    [DataContract]
    class ServerConfig
    {
        [DataMember(Name = "thisServerIP")]
        public string thisServerIP;

        [DataMember(Name = "listenDSCPort")]
        public int listenDSCPort;

        [DataMember(Name = "listenPlayerPort")]
        public int listenPlayerPort;

        [DataMember(Name = "dscSecretKey")]
        public string dscSecretKey;

        [DataMember(Name = "playerSecretKey")]
        public string playerSecretKey;

        public ServerConfig()
        {

        }

        public static void Convert(string data, out ServerConfig targetInstance)
        {
            targetInstance = new ServerConfig();
            MemoryStream ms = new MemoryStream(Encoding.ASCII.GetBytes(data));
            DataContractJsonSerializer jsonData = new DataContractJsonSerializer(targetInstance.GetType());
            targetInstance = jsonData.ReadObject(ms) as ServerConfig;
        }
    }
}
